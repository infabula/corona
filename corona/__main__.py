# this will run if you run 'python -m corona'
import sys
from corona.corona import main
from corona.microscope import Microscope, MicroscopeGlassError


def main():
    micro = Microscope()
    micro._magnification_factor = -400.0  # unsafe
    micro.set_magnification_factor(-400.0) # safe 
    micro.magnification_factor = 400.0  # pythonic property

    try:
        micro.add_glass()
    except MicroscopeGlassError as e:
        print(e)

    
    print("mag factor", micro.magnification_factor)


    mic_2 = Microscope()
    mic_3 = Microscope()
    del(mic_2)
    del(mic_3)
    sys.exit(0)
    print("There were", Microscope.object_count, "microscopes created") 
    
if __name__ == "__main__":
    main()
