from .person import Person

class DocterSkillException(Exception):
    pass


class Docter(Person):
    def __init__(self, name, specialism="arts"):
        super().__init__(name)
        self.specialism = specialism


    def do_surgery(self):
        '''Let the docter do a surgery.

        Raises DocterSkillException if docter is not skilled.
        '''
        if self.specialism == "chirurg":
            return "Saved the pataten"
        else:
            raise DocterSkillException("Kan niet opereren")
