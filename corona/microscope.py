'''Microscope module '''

class MicroscopeGlassError(Exception):
    pass


class Microscope():
    '''Used for viewing small stuff'''
    
    object_count = 0  # class property
    current_count = 0
    
    def __init__(self):
        print("Creating new Microscope")
        Microscope.object_count += 1
        self._magnification_factor = 10.0
        self._max_magnification = 200.0
        self._min_magnification = 1.0
        self._has_object_glass = False

    def __del__(self):
        print("I'm gone!")

        
    def get_magnification_factor(self):
        return self._magnification_factor

    def set_magnification_factor(self, factor):
        # sanitize input
        if factor < self._min_magnification:
            self._magnification_factor = self._min_magnification
        elif factor > self._max_magnification:
            self._magnification_factor = self._max_magnification

    def add_glass(self):
        '''Add a glass.

        Raises an Exception if there is already a glass on microscope.'''
        if self._has_object_glass:
            raise MicroscopeGlassError("There is already a glass on the mic")
        

    def remove_glass(self):
        ''' Remove glass object from microscope.

        Raises an Exception if there is no glass on the microscope.'''

    def zoom_in(self):
        '''Multiplies the magnification factor by two.
        
        If it reaches the max magnification factor it sets it to the mac magni...
        '''
        

    def zoom_out(self):
        pass

    magnification_factor = property(get_magnification_factor, set_magnification_factor)
        
