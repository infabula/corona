import unittest
from corona import Docter, DocterSkillException


class TestDocter(unittest.TestCase):

    def test_docter_construction(self):
        john = Docter('John')  # setup
        self.assertEqual(john.name, 'John')
        self.assertEqual(john.specialism, 'arts')


    def test_exceptional_surgery(self):
        john = Docter('John')
        with self.assertRaises(DocterSkillException):
            john.do_surgery()
        

    def test_can_do_surgery(self):
        john = Docter('John', 'chirurg')
        self.assertEqual(john.do_surgery(), "Saved the patient")
