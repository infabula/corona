from setuptools import setup, find_packages


setup(
    name='corona',
    version='0.0.2',
    url='https://example.org',
    author='Anonymous',
    author_email='anonymous@example.org',
    packages=['corona']
)
